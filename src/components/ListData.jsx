import React, { useState } from 'react';
import {PieChart, Pie, Sector, BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
// import { PieChart, Pie, Sector, Cell, ResponsiveContainer } from 'recharts';


export default function ListData() {
    // pie chart
    const record = [
        { name: 'Group A', value: 400 },
        { name: 'Group B', value: 300 },
        { name: 'Group C', value: 300 },
        { name: 'Group D', value: 200 },
      ];
      
const COLORS = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];

const RADIAN = Math.PI / 180;
const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
  const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
  const x = cx + radius * Math.cos(-midAngle * RADIAN);
  const y = cy + radius * Math.sin(-midAngle * RADIAN);

  return (
    <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
      {`${(percent * 100).toFixed(0)}%`}
    </text>
  );
};

  // Bar chart
const [data,setData] = useState([

]);
const [data1,setData1] = useState([])

    const save =(e)=>{
        debugger
e.preventDefault();
console.log(data)
setData1([data]);
    }
    const clearAll =()=>{
        debugger
        setData1([])
        setData([])
    }
  return (
    <div>
      <form onSubmit={save}>
          <div className="m-5">
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">
           Age
          </label>
          <input
            type="number"
            max={100}
            class="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            value={data.age}
            onChange={(e) =>
                setData({
                  ...data,
                  age: e.target.value,
                })
              }
          />
         
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">
            Weight
          </label>
          <input
            type="number"
            min={1}
            max={100}
            class="form-control"
            id="exampleInputPassword1"
            value={data.weight}
            onChange={(e) =>
                setData({
                  ...data,
                  weight: e.target.value,
                })
              }
          />
        </div>
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">
            Height
          </label>
          <input
            type="number"
            min={1}
            max={200}
            class="form-control"
            id="exampleInputPassword1"
            value={data.height}
            onChange={(e) =>
                setData({
                  ...data,
                  height: e.target.value,
                })
              }
          />
        </div>
        <button  class="btn btn-primary" onClick={clearAll}>Clear</button>{" "}
        <button type="submit" class="btn btn-primary">
          Submit
        </button>
        </div>
      </form>
      <div className='row'>
          <div className='col-md-6'>
          <BarChart
          width={500}
          height={300}
          data={data1}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="weight" fill="#8884d8" />
          <Bar dataKey="age" fill="#82ca9d" />
          <Bar dataKey="height" fill="#ffc658" />
        </BarChart>
          </div>
          {/* <div className='col-md-6'>
          <PieChart width={400} height={400}>
          <Pie
            data={record}
            cx="50%"
            cy="50%"
            labelLine={false}
            label={renderCustomizedLabel}
            outerRadius={80}
            fill="#8884d8"
            dataKey="value"
          >
            {data.map((entry, index) => (
              <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
            ))}
          </Pie>
        </PieChart>
          </div> */}
       
      </div>
    </div>
  );
}
