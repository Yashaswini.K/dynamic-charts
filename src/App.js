import logo from './logo.svg';
import './App.css';
import ListData from './components/ListData';

function App() {
  return (
    <div className="App">
     <ListData/>
    </div>
  );
}

export default App;
